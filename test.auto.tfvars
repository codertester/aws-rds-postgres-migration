# ******************************************************************************************************************
# *  test.auto.tfvars                                                                                              *
# ******************************************************************************************************************
# *                                                                                                                *
# *  Project: Migration Project                                                                                    *
# *                                                                                                                *
# *  Copyright © 2021 Codetester. All Rights Reserved.                           								   *
# *                                                                                                                *
# *                                                                                                                *
# ******************************************************************************************************************


# 0. organization identifier, project, creator environmental, region, and tagging variables
org_identifier                            = "org"
project_name                              = "mgr"
region                                    = "us-east-1"
creator                                   = "Terraform"
environment                               = "test"
map_dba									  = "map-dba"

# 1. db subnet group variables
db_subnet_group_description               = "Mgr DB Subnet Group"
db_subnet_group_name                      = "db-subnet-group"
db_subnet_ids                             = ["subnet-0d141dd23929f0eb3", "subnet-09d9ce03690b79111"]

# 2. db parameter group variables
db_parameter_group_name                   = "db-parameter-group"
db_parameter_group_family                 = "postgres13"
db_parameter_group_description            = "Mgr DB Parameter Group"
db_parameter_name_one 					  = "shared_preload_libraries"
db_parameter_name_one_value            	  = "pg_stat_statements, pg_cron"
db_parameter_group_apply_method           = "pending-reboot"

# 3. security group variables
security_group_name                       = "postgres-sg"
security_group_description                = "Allow SSH and DB ports"
vpc_id                                    = "vpc-06e79d3c234efea71"
ingress_ssh_description                   = "SSH Access"
ingress_ssh_port                          = 22
ingress_protocol                          = "tcp"
ingress_cidr_blocks                       = ["172.31.32.0/20"]
ingress_db_description                    = "Postgres Access"
ingress_db_port                           = 5432
egress_port                               = 0
egress_protocol                           =  "-1"
egress_cidr_blocks                        = ["0.0.0.0/0"]

# 4. credentials-related  variables i.e. variables for random password, secret random uuid, secret & secret version
random_password_length                    = 20
random_password_true                      = true
random_password_override_special          = "!#$&*()-_=[]{}<>:?"
recovery_window_in_days                   = 7
secret_description                        = "Mgr Postgres Credentials"
secret_tag_value                          = "mgr-postgres-credentials"
aws_secretsmanager_secret_name            = "mgr-postgres-secret"
username                                  = "postgres"

# 5. rds monitoring role variable(s)
rds_monitoring_role_name                  = "rds-monitoring-role"

# 6. instance variables
database_name                             = "migrationDB"
instance_identifier                       = "mgr"
allocated_storage                         = 200
max_allocated_storage                     = 500
storage_type                              = "gp2"
storage_encrypted                         = true
engine                                    = "postgres"
engine_version                            = "13.4"
publicly_accessible                       = false
instance_class                            = "db.t3.micro"
allow_major_version_upgrade               = true
auto_minor_version_upgrade                = true
apply_immediately                         = true
monitoring_interval                       = 60
backup_retention_period                   = 30
preferred_backup_window                   = "20:05-22:35"
preferred_maintenance_window              = "sun:12:05-sun:14:35"
performance_insights_enabled              = true
copy_tags_to_snapshot                     = true
deletion_protection                       = true
enabled_cloudwatch_logs_exports           = ["postgresql", "upgrade"]
skip_final_snapshot                       = false
final_snapshot_identifier                 = "mgr-snapshot"
multi_az                                  = false
performance_insights_retention_period     = 7