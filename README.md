# README #

This README summaries the contents of this repository.

### What is this repository for? ###

* Deployment of AWS RDS PostgreSQL DB instance and related resources for the migration on an Oracle database to RDS PostgreSQL DB instance.
* Version 1.0

### Deployed resoures ###
* AWS RDS DB subnet group
* AWS RDS DB parameter group
* AWS VPC security group 
* AWS secret manager secret
* AWS RDS enhanced monitoring IAM role 
* AWS RDS PostgreSQL DB instance 

### Repo usage ###

* Database migration
* Deployment instructions: use bitbucket-pipeline 
* Deployment yml file: **bitbucket-pipelines.yml** 

### Contribution guidelines ###

* Create branch
* Add codes
* Submit pull request for code review

### Who do I talk to? ###

* Repo owner or admin

--- 
 * [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)